package com.stoke.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import com.stoke.persistence.util.JPAUtil;
import com.stoke.persistence.util.Parameter;

public class DAO<T> {
	private Class<T> clazz;

    public DAO(Class<T> clazz) {
        this.clazz = clazz;
    }

    public void insert(T entity){
        EntityManager manager = JPAUtil.getEntityManager();
        manager.getTransaction().begin();
        manager.persist(entity);
        manager.getTransaction().commit();
        manager.close();
    }
    public List<T> listAll(){
        EntityManager manager = JPAUtil.getEntityManager();
        CriteriaQuery<T> query = manager.getCriteriaBuilder().createQuery(clazz);
        query.select(query.from(clazz));
        List<T> result = manager.createQuery(query).getResultList();
        manager.close();
        return  result;
    }
   
    public void delete(T entity){
    	EntityManager manager = JPAUtil.getEntityManager();
		manager.getTransaction().begin();
		manager.remove(manager.merge(entity));
		manager.getTransaction().commit();
		manager.close();
    }
    
    public void update(T entity){
        EntityManager manager = JPAUtil.getEntityManager();
        manager.getTransaction().begin();
        manager.merge(entity);
        manager.getTransaction().commit();
        manager.close();
    }

    public T findbyId(Integer id){
        EntityManager manager = JPAUtil.getEntityManager();
        T result = manager.find(clazz, id);
        manager.close();
        return result;
    }
    public List<T> listByQuery(String queryName,List<Parameter> params){
        EntityManager manager = JPAUtil.getEntityManager();
        Query query = manager.createNamedQuery(queryName);
        for(Parameter param:params){
        	if(!param.isStringConcat())
        		query.setParameter(param.getKey(), param.getValue());
        	else query.setParameter(param.getKey(), "%"+param.getValue()+"%");
        }
        List<T> result = query.getResultList();
        manager.close();
        return result;
    }
    
    public T findByQuery(String queryName,List<Parameter> params){
        try {
            EntityManager manager = JPAUtil.getEntityManager();
            Query query = manager.createNamedQuery(queryName).setMaxResults(1);
            for (Parameter param : params){
            	if(!param.isStringConcat())
            		query.setParameter(param.getKey(), param.getValue());
            	else query.setParameter(param.getKey(), "%"+param.getValue()+"%");
            	
            }
            T result = (T)query.getSingleResult();
            manager.close();
            return result;
        }catch (NoResultException ex){
            return null;
        }
    }
}
