package com.stoke.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ProdutoItem{
	@Id @GeneratedValue
	private Integer id;
	@ManyToOne
	private Produto produto;
	@ManyToOne
	private Venda venda;
	private Integer quantidade = 0;
	
	public ProdutoItem() {
		
	}
	public ProdutoItem(Produto produto) {
		this.produto = produto;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Venda getVenda() {
		return venda;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj==null)
			return false;
		else{
			ProdutoItem item = (ProdutoItem)obj;
			if(item.getProduto()==null)
				return false;
			else{
				Produto produto = item.getProduto();
				if(produto.getCodigo()==null)
					return false;
				else
					return produto.getCodigo().equals(this.getProduto().getCodigo());
			}
		}
		
	}	
	
}
