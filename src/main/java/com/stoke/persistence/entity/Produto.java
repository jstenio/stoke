package com.stoke.persistence.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Produto {
	
	@Id @GeneratedValue 
	private Integer id;
	@Column(nullable=false)
	private String nome;
	private String descricao;
	@Column(nullable=false)
	private BigDecimal valor;
	@Column(unique=true,nullable=false)
	private String codigo;
	private String imagem;
	private Integer quantidade;
	@OneToMany(mappedBy="produto")
	private List<ProdutoItem> produtoItens;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
		
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public List<ProdutoItem> getProdutoItens() {
		return produtoItens;
	}
	public void setProdutoItens(List<ProdutoItem> produtoItens) {
		this.produtoItens = produtoItens;
	}
	
	
}	
