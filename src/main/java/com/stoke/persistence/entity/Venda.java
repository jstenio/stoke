package com.stoke.persistence.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Venda {
	@Id @GeneratedValue
	private Integer id;
	@ManyToOne
	private Cliente cliente;
	@OneToMany(mappedBy="venda",cascade={CascadeType.PERSIST,CascadeType.REMOVE,CascadeType.REFRESH,CascadeType.MERGE})
	private List<ProdutoItem> itens;
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar data;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public List<ProdutoItem> getItens() {
		return itens;
	}
	public void setItens(List<ProdutoItem> itens) {
		this.itens = itens;
	}
	public Calendar getData() {
		return data;
	}
	public void setData(Calendar data) {
		this.data = data;
	}
	
}
