package com.stoke.persistence.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;

@Entity
public class Cliente extends Usuario{
	
	@OneToOne(mappedBy="cliente",cascade={CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REMOVE})
	private Endereco endereco = new Endereco();
	@OneToMany(mappedBy="cliente",cascade={CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REMOVE})
	private List<Telefone> telefones;
	
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public List<Telefone> getTelefones() {
		return telefones;
	}
	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}
	
	
	
	
}
