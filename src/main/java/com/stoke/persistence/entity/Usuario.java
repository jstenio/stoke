package com.stoke.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@NamedQuery(name="userByEmailAndPassword",query="SELECT u FROM Usuario u WHERE u.email=:pEmail AND u.senha=:pSenha")
public abstract class Usuario {
	
	@Id @GeneratedValue
	private Integer id;
	@Column(nullable=false)
	private String nome;
	@Column(nullable=false,unique=true)
	private String email;
	@Column(nullable=false)
	private String senha;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	
}
